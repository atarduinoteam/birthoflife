#include <unity.h>
#include <HysteresisRound.h>

void testNegativeValuesWillBeRoundedToZero() {
    TEST_ASSERT_EQUAL(0, hysteresisRound(-1, 53));
    TEST_ASSERT_EQUAL(0, hysteresisRound(53, -1));
    TEST_ASSERT_EQUAL(0, hysteresisRound(-1, -5));
}

void testRoundToMinimal() {
    TEST_ASSERT_EQUAL(50, hysteresisRound(10, 50));
    TEST_ASSERT_EQUAL(50, hysteresisRound(10, 51));
    TEST_ASSERT_EQUAL(50, hysteresisRound(10, 52));
    TEST_ASSERT_EQUAL(50, hysteresisRound(10, 53));
}

void testRoundToMaximal() {
    TEST_ASSERT_EQUAL(60, hysteresisRound(10, 56));
    TEST_ASSERT_EQUAL(60, hysteresisRound(10, 57));
    TEST_ASSERT_EQUAL(60, hysteresisRound(10, 58));
    TEST_ASSERT_EQUAL(60, hysteresisRound(10, 59));
}

void testRoundToPrevious() {
    TEST_ASSERT_EQUAL(50, hysteresisRound(50, 54));
    TEST_ASSERT_EQUAL(50, hysteresisRound(50, 55));
    TEST_ASSERT_EQUAL(60, hysteresisRound(60, 54));
    TEST_ASSERT_EQUAL(60, hysteresisRound(60, 55));
}

void testRoundToMinimalInsteadOfPreviousInGreyZone() {
    TEST_ASSERT_EQUAL(50, hysteresisRound(40, 54));
    TEST_ASSERT_EQUAL(50, hysteresisRound(70, 54));
}

void testRoundToMaximalInsteadOfPreviousInGreyZone() {
    TEST_ASSERT_EQUAL(60, hysteresisRound(40, 55));
    TEST_ASSERT_EQUAL(60, hysteresisRound(70, 55));
}

int main(int argc, char **argv) {
    UNITY_BEGIN();
    RUN_TEST(testNegativeValuesWillBeRoundedToZero);
    RUN_TEST(testRoundToMinimal);
    RUN_TEST(testRoundToMaximal);
    RUN_TEST(testRoundToPrevious);
    RUN_TEST(testRoundToMinimalInsteadOfPreviousInGreyZone);
    RUN_TEST(testRoundToMaximalInsteadOfPreviousInGreyZone);
    UNITY_END();
}
