#include <unity.h>
#include <EepromConfig.h>

void testInit() {
    EepromConfig* eC = new EepromConfig(18);
    TEST_ASSERT_EQUAL(18, eC -> getVersion());
    TEST_ASSERT_EQUAL(0, eC -> getStartTime());
    TEST_ASSERT_EQUAL(DEFAULT_TEMP_PRESET, eC-> getPresetTemp());
    TEST_ASSERT_EQUAL(DEFAULT_HUMIDITY_PRESET, eC-> getPresetHumidity());
    delete eC;
}

void testStartTimeSaved() {
    EepromConfig* eC = new EepromConfig(18);
    eC->setStartTime(151);
    delete eC;
    eC = new EepromConfig(18);
    TEST_ASSERT_EQUAL(151, eC -> getStartTime());
    delete eC;
}

void testTempPresetSaved() {
    EepromConfig* eC = new EepromConfig(18);
    eC->setPresetTemp(300);
    delete eC;
    eC = new EepromConfig(18);
    TEST_ASSERT_EQUAL(300, eC -> getPresetTemp());
    delete eC;
}

void testHumidityPresetSaved() {
    EepromConfig* eC = new EepromConfig(18);
    eC->setPresetHumidity(500);
    delete eC;
    eC = new EepromConfig(18);
    TEST_ASSERT_EQUAL(500, eC -> getPresetHumidity());
    delete eC;
}

int main(int argc, char **argv) {
    UNITY_BEGIN();
    RUN_TEST(testInit);
    RUN_TEST(testStartTimeSaved);
    RUN_TEST(testTempPresetSaved);
    RUN_TEST(testHumidityPresetSaved);
    UNITY_END();
}
