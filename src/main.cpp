#include <NextionComponents.h>
#include <LightController.h>
#include <RtcController.h>
#include <MainScreenController.h>
#include <TimerScreenController.h>
#include <ConfigTempScreenController.h>
#include <TemperatureSensor.h>
#include <HumidityController.h>
#include <TemperaturePIDRegulator.h>
#include <CommonConfiguration.h>
#include <HumidityPIDRegulator.h>
#include <AerationController.h>

// @file NexHardware.cpp

NexTouch *touchListenList[] = {
  &lightBtn,
  &resetTimerBtn,
  &cancelResetTimerBtn,
  &tempConfBtn,
  &tcSubBtn,
  &tcAddBtn,
  &tcOkBtn,
  &tcCancelBtn,
  NULL
};

BME280I2C _bme;

void setup() {
  nexInit();
  lightController -> init();
  rtcController -> init();
  mainScreenController -> init();
  timerScreenController -> init();
  configTempScreenController -> init();
  t1 -> init();
  t2 -> init();
  tEgg -> init();
  humidityController -> init();
  temperaturePIDRegulator -> init(TEMPERATURE_KP, TEMPERATURE_KI, TEMPERATURE_KD, TEMPERATURE_DT);
  humidityPIDRegulator -> init(HUMIDITY_KP, HUMIDITY_KI, HUMIDITY_KD, HUMIDITY_DT);
}

void loop() {
  rtcController -> process();
  t1 -> process();
  t2 -> process();
  tEgg -> process();
  humidityController -> process();
  temperaturePIDRegulator -> process();
  humidityPIDRegulator -> process();
  aerationController -> process();
  nexLoop(touchListenList);
}
