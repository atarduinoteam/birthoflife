#ifndef BOL_LOW_FREQUENCY_PWM_h
#define BOL_LOW_FREQUENCY_PWM_h

#include <Arduino.h>

/**
 * period - ms
 * steps
*/
class LowFrequencyPWM {
  public:
    LowFrequencyPWM(uint8_t pin, uint16_t stepDuration, uint8_t stepsCount, bool reverse);
    void setDutyCycle(uint16_t dutyCycle);
    void process();
  private:
    uint8_t _pin;
    uint16_t _stepDuration;
    uint8_t _stepsCount;
    uint16_t _dutyCycle;
    unsigned long _lastStepTime;
    uint8_t _currentDutyCycle;
    bool _dutyCycleChanged;
    bool _reverse;
};

#endif