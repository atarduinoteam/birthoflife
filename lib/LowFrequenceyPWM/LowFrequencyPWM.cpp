#include <LowFrequencyPWM.h>

LowFrequencyPWM::LowFrequencyPWM(uint8_t pin, uint16_t stepDuration, uint8_t stepsCount, bool reverse) {
    _pin = pin;
    _stepDuration = stepDuration;
    _stepsCount = stepsCount;
    _lastStepTime = millis();
    _currentDutyCycle = 0;
    _dutyCycleChanged = false;
    _reverse = reverse;
    setDutyCycle(0);
    pinMode(_pin, OUTPUT);
    digitalWrite(_pin, LOW);
}

void LowFrequencyPWM::setDutyCycle(uint16_t dutyCycle) {
    if (_reverse) {
        _dutyCycle = constrain(_stepsCount - dutyCycle, 0, _stepsCount);
    } else {
        _dutyCycle = constrain(dutyCycle, 0, _stepsCount);
    }
    _dutyCycleChanged = true;
}

void LowFrequencyPWM::process() {
    if ((millis() - _lastStepTime) >= _stepDuration) {
        _lastStepTime = millis();
        _currentDutyCycle++;
        if (_currentDutyCycle == _stepsCount) {
            _currentDutyCycle = 0;
        }
        if (_dutyCycleChanged) {
            if (_currentDutyCycle < _dutyCycle) {
                digitalWrite(_pin, HIGH);
            }
            _dutyCycleChanged = false;
        }
        if ((_currentDutyCycle == 0) && (_dutyCycle != 0)) {
            digitalWrite(_pin, HIGH);
        } else if ((_currentDutyCycle == _dutyCycle) && (_stepsCount != _dutyCycle)) {
            digitalWrite(_pin, LOW);
        }
    }
}
