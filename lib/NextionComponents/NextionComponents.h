#ifndef NextionComponents_h
#define NextionComponents_h

#include <Nextion.h>

// Main page
extern NexPage mainPage;

extern NexText timerText;
extern NexText dateTimeText;

extern NexText t1Text;
extern NexText t2Text;
extern NexText tEggText;
extern NexText tSetText;
extern NexText hTxt;
extern NexText hSetTxt;

extern NexButton lightBtn;
extern NexHotspot tempConfBtn;

// Reset timer page
extern NexButton resetTimerBtn;
extern NexButton cancelResetTimerBtn;

// Configure temp page
extern NexPage tempConfPage;
extern NexText tempPresetText;
extern NexButton tcSubBtn;
extern NexButton tcAddBtn;
extern NexButton tcOkBtn;
extern NexButton tcCancelBtn;

#endif