#include <NextionComponents.h>

// Main page
NexPage mainPage = NexPage(0, 0, "main");

NexText timerText = NexText(0, 1, "timerTxt");
NexText dateTimeText = NexText(0, 2, "dateTimeTxt");

NexText t1Text = NexText(0, 3, "t1Txt");
NexText t2Text = NexText(0, 4, "t2Txt");
NexText tEggText = NexText(0, 5, "tEggTxt");
NexText tSetText = NexText(0, 6, "tSetTxt");
NexText hTxt = NexText(0, 7, "hTxt");
NexText hSetTxt = NexText(0, 8, "hSetTxt");

NexButton lightBtn = NexButton(0, 13, "lightBtn");
NexHotspot tempConfBtn = NexHotspot(0, 15, "tempConfBtn");

// Reset timer page
NexButton resetTimerBtn = NexButton(1, 1, "rstBtn");
NexButton cancelResetTimerBtn = NexButton(1, 2, "cancelBtn");

// Configure temp page
NexPage tempConfPage = NexPage(2, 0, "TempConf");
NexText tempPresetText = NexText(2, 1, "tempSetTxt");
NexButton tcSubBtn = NexButton(2, 2, "subBtn");
NexButton tcAddBtn = NexButton(2, 3, "addBtn");
NexButton tcOkBtn = NexButton(2, 4, "okBtn");
NexButton tcCancelBtn = NexButton(2, 5, "cancelBtn");