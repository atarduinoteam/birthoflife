#include <ConfigTemperatureController.h>
#include <NextionComponents.h>
#include <EepromConfig.h>

ConfigTemperatureController* configTemperatureController = new ConfigTemperatureController();

ConfigTemperatureController::ConfigTemperatureController() {
    
}

void ConfigTemperatureController::init() {
    _configuredTemperature = eepromConfig -> getPresetTemp();
}

void ConfigTemperatureController::resetConfiguredTemperature() {
    _configuredTemperature = eepromConfig -> getPresetTemp();
}

int ConfigTemperatureController::getConfiguredTemperature() {
    return _configuredTemperature;
}

void ConfigTemperatureController::increaseConfiguredTemperature() {
    if (_configuredTemperature < 500) {
        _configuredTemperature++;
    }
}

void ConfigTemperatureController::decreaseConfiguredTemperature() {
    if (_configuredTemperature > 250) {
        _configuredTemperature--;
    }
}

void ConfigTemperatureController::saveConfiguredTemperature() {
    eepromConfig -> setPresetTemp(_configuredTemperature);
}
