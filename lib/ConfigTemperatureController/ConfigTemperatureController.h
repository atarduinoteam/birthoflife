#ifndef BOL_Config_Temperature_Controller_h
#define BOL_Config_Temperature_Controller_h

class ConfigTemperatureController {
    public:
        ConfigTemperatureController();
        void init();
        void resetConfiguredTemperature();
        int getConfiguredTemperature();
        void increaseConfiguredTemperature();
        void decreaseConfiguredTemperature();
        void saveConfiguredTemperature();
    private:
        int _configuredTemperature;
};

extern ConfigTemperatureController* configTemperatureController;

#endif