#include <EepromConfig.h>
#include <EEPROM.h>
#include <CommonConfiguration.h>

EepromConfig* eepromConfig = new EepromConfig(SOFTWARE_VERSION);

EepromConfig::EepromConfig(uint8_t version) {
    EEPROM.get(0, _configurationStructure);
    if (_configurationStructure.version != version) {
        _initStruct(version);
    }
}

uint8_t EepromConfig::getVersion() {
    return _configurationStructure.version;
}

time_t EepromConfig::getStartTime() {
    return _configurationStructure.startTime;
}

int EepromConfig::getPresetTemp() {
    return _configurationStructure.presetTemp;
}

int EepromConfig::getPresetHumidity() {
    return _configurationStructure.presetHumidity;
}

void EepromConfig::setVersion(uint8_t version) {
    _configurationStructure.version = version;
    EEPROM.put(0, _configurationStructure);
}

void EepromConfig::setStartTime(time_t startTime) {
    _configurationStructure.startTime = startTime;
    EEPROM.put(0, _configurationStructure);
}

void EepromConfig::setPresetTemp(int presetTemp) {
    _configurationStructure.presetTemp = presetTemp;
    EEPROM.put(0, _configurationStructure);
}

void EepromConfig::setPresetHumidity(int presetHumidity) {
    _configurationStructure.presetHumidity = presetHumidity;
    EEPROM.put(0, _configurationStructure);
}

void EepromConfig::_initStruct(uint8_t version) {
    _configurationStructure.version = version;
    _configurationStructure.startTime = 0;
    _configurationStructure.presetTemp = DEFAULT_TEMP_PRESET;
    _configurationStructure.presetHumidity = DEFAULT_HUMIDITY_PRESET;
    EEPROM.put(0, _configurationStructure);
}
