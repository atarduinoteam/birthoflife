#ifndef BOL_EepromConfig_h
#define BOL_EepromConfig_h

#include <Arduino.h>
#include <TimeLib.h>

#define DEFAULT_TEMP_PRESET 370
#define DEFAULT_HUMIDITY_PRESET 530

typedef struct {                            // Configuration structure
  uint8_t version;                          // version of data structure
  time_t startTime;                         // Start time for timer. Reset timer should update this value
  int presetTemp;
  int presetHumidity;
} __attribute__((packed)) ConfigurationStructure;

class EepromConfig {
  public:
    EepromConfig(uint8_t version);
    uint8_t getVersion();
    time_t getStartTime();
    int getPresetTemp();
    int getPresetHumidity();
    void setVersion(uint8_t version);
    void setStartTime(time_t startTime);
    void setPresetTemp(int presetTemp);
    void setPresetHumidity(int presetHumidity);
  private:
    ConfigurationStructure _configurationStructure;
    void _initStruct(uint8_t version);
};

extern EepromConfig* eepromConfig;

#endif
