#include <AerationController.h>
#include <Arduino.h>
#include <CommonConfiguration.h>

AerationController* aerationController = new AerationController();

AerationController::AerationController() {
    _period = DEFAULT_AERATION_PERIOD;
    _duration = DEFAULT_AERATION_DURATION;
    _prevExecutionTime = millis();
    _fanTurnedOn = false;
    pinMode(EXT_FAN_PIN, OUTPUT);
    digitalWrite(EXT_FAN_PIN, LOW);
}

void AerationController::setPeriod(int newPeriod) {
    _period = newPeriod * 60000;
}

void AerationController::setDuration(int newDuration){
    _duration = newDuration * 1000;
}

void AerationController::process() {
    if ((millis() - _prevExecutionTime) >= _period) {
        _prevExecutionTime = millis();
        turnOnFan();
    }
    if ((millis() - _prevExecutionTime) >= _duration) {
        turnOffFan();
    }
}

void AerationController::turnOnFan() {
    if (!_fanTurnedOn) {
        digitalWrite(EXT_FAN_PIN, HIGH);
        _fanTurnedOn = true;
    }
}

void AerationController::turnOffFan() {
    if (_fanTurnedOn) {
        digitalWrite(EXT_FAN_PIN, LOW);
        _fanTurnedOn = false;
    }
}
