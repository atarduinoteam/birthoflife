#ifndef BOL_AerationController_h
#define BOL_AerationController_h

class AerationController {
  public:
    AerationController();
    void setPeriod(int newPeriod); // minutes
    void setDuration(int newDuration); // seconds
    void process();
  private:
    void turnOnFan();
    void turnOffFan();
    unsigned long _period;
    unsigned long _duration;
    unsigned long _prevExecutionTime;
    bool _fanTurnedOn;
};

extern AerationController* aerationController;

#endif