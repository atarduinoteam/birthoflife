#include <LightController.h>
#include <NextionComponents.h>
#include <CommonConfiguration.h>

LightController* lightController = new LightController();

LightController::LightController() {

}

void LightController::init() {
    pinMode(LIGHT_PIN, OUTPUT);
    digitalWrite(LIGHT_PIN, LOW);
    _isLightOn = false;
}

void LightController::lightSwitch() {
    _isLightOn = !_isLightOn;
    digitalWrite(LIGHT_PIN, _isLightOn ? HIGH : LOW);
}

bool LightController::isLightOn() {
    return _isLightOn;
}
