#ifndef LightController_h
#define LightController_h

class LightController {
  public:
    LightController();
    void init();
    void lightSwitch();
    bool isLightOn();
  private:
    bool _isLightOn;
};

extern LightController* lightController;

#endif