#include <TemperatureSensor.h>
#include <MainScreenController.h>
#include <HysteresisRound.h>

TemperatureSensor* t1 = new TemperatureSensor(0x48, T1);
TemperatureSensor* t2 = new TemperatureSensor(0x49, T2);
TemperatureSensor* tEgg = new TemperatureSensor(0x4A, T_EGG);

void temperatureUpdated(void* temperatureSensorPtr) {
    TemperatureSensor* temperatureSensor = (TemperatureSensor*) temperatureSensorPtr;
    mainScreenController -> updateTemperatureValue(temperatureSensor);
}

TemperatureSensor::TemperatureSensor(uint8_t address, TemperatureSensorType temperatureSensorType) {
    _tmp117 = new TMP117(address);
    _temperatureSensorType = temperatureSensorType;
    _value = 0;
}

TemperatureSensor::~TemperatureSensor() {
    delete _tmp117;
}

void TemperatureSensor::init() {
    _tmp117 -> init(temperatureUpdated, this);
    _tmp117 ->setConvTime(C4S);
    _tmp117 ->setAveraging(AVE64);
}

void TemperatureSensor::process() {
    _tmp117 -> update();
}

TemperatureSensorType TemperatureSensor::getTemperatureSensorType() {
    return _temperatureSensorType;
}

char* TemperatureSensor::getValue() {
    int value = (_tmp117 -> getTemperature()) * 100;
    _value = hysteresisRound(_value, value);
    return convertSensorValueToStr(_value / 10);
}

int TemperatureSensor::getNumericValue() {
    return (_tmp117 -> getTemperature()) * 100;
}