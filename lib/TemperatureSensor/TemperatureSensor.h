#ifndef BOL_Temperature_Sensor_h
#define BOL_Temperature_Sensor_h

#include <TMP117.h>

enum TemperatureSensorType {
    T1 = 0, 
    T2, 
    T_EGG};  

class TemperatureSensor {
    public:
        TemperatureSensor(uint8_t address, TemperatureSensorType temperatureSensorType);
        ~TemperatureSensor();
        void init();
        void process();
        TemperatureSensorType getTemperatureSensorType();
        char* getValue();
        int getNumericValue();
    private:
        TMP117* _tmp117;
        TemperatureSensorType _temperatureSensorType;
        int _value = 0;
};

extern TemperatureSensor* t1;
extern TemperatureSensor* t2;
extern TemperatureSensor* tEgg;

#endif