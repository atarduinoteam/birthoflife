#include <PIDRegulator.h>
#include <Arduino.h>

PIDRegulator::PIDRegulator() {
    _prevExecutionTime = millis();
    _integral = 0;
    _prevErr = 0;
}

void PIDRegulator::init(float kp, float ki, float kd, unsigned long dt) {
    _kp = kp;
    _ki = ki;
    _kd = kd;
    _dt = dt;
}

void PIDRegulator::setTargetPoint(int newTargetPoint) {
    _targetPoint = newTargetPoint;
}

void PIDRegulator::process() {
    if ((millis() - _prevExecutionTime) >= _dt) {
        _prevExecutionTime = millis();
        control(computePID(getSensorValue(), 0, 250));
    }
}

int PIDRegulator::computePID(int input, int minOut, int maxOut) {
    int err = _targetPoint - input;
    _integral = constrain(_integral + (float)err * (_dt / 1000.0) * _ki, minOut, maxOut);
    float d = (err*1.0 - _prevErr) / (_dt/1000.0);
    _prevErr = err*1.0;
    return constrain(round(err*1.0 * _kp + _integral + d * _kd), minOut, maxOut);
}