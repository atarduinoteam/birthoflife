#ifndef BOL_PIDRegulator_h
#define BOL_PIDRegulator_h

class PIDRegulator {
  public:
    PIDRegulator();
    void init(float kp, float ki, float kd, unsigned long dt);
    void setTargetPoint(int newTargetPoint);
    virtual void process();
  private:
    virtual int getSensorValue() = 0;
    virtual void control(int newControlSignal) = 0;
    int computePID(int input, int minOut, int maxOut);
    int _targetPoint;
    float _kp;
    float _ki;
    float _kd;
    unsigned long _dt;
    unsigned long _prevExecutionTime;
    float _integral;
    float _prevErr;
};

#endif