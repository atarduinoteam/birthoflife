#include <ConfigTempScreenController.h>
#include <NextionComponents.h>
#include <ConfigTemperatureController.h>
#include <MainScreenController.h>
#include <HysteresisRound.h>

ConfigTempScreenController* configTempScreenController = new ConfigTempScreenController();

void tempConfigSubBtnHandler(void* ptr) {
    configTemperatureController -> decreaseConfiguredTemperature();
    configTempScreenController -> updateTemperatureValue();
}

void tempConfigAddBtnHandler(void* ptr) {
    configTemperatureController -> increaseConfiguredTemperature();
    configTempScreenController -> updateTemperatureValue();
}

void tempConfigSaveBtnHandler(void* ptr) {
    configTemperatureController -> saveConfiguredTemperature();
    mainPage.show();
    mainScreenController -> redrawMainScreen();
}

void tempConfigCancelBtnHandler(void* ptr) {
    configTemperatureController -> resetConfiguredTemperature();
    mainPage.show();
    mainScreenController -> redrawMainScreen();
}

ConfigTempScreenController::ConfigTempScreenController() {

}

void ConfigTempScreenController::init() {
    configTemperatureController -> init();
    tcSubBtn.attachPop(tempConfigSubBtnHandler, this);
    tcAddBtn.attachPop(tempConfigAddBtnHandler, this);
    tcOkBtn.attachPop(tempConfigSaveBtnHandler, this);
    tcCancelBtn.attachPop(tempConfigCancelBtnHandler, this);
}

void ConfigTempScreenController::redrawConfigureTempScreen() {
    configTemperatureController -> resetConfiguredTemperature();
    updateTemperatureValue();
}

void ConfigTempScreenController::updateTemperatureValue() {
    int newValue = configTemperatureController -> getConfiguredTemperature();
    char* presetTempStr = convertSensorValueToStr(newValue);
    tempPresetText.setText(presetTempStr);
    delete[] presetTempStr;
}
