#ifndef BOL_ConfigTempScreenController_h
#define BOL_ConfigTempScreenController_h

class ConfigTempScreenController {
  public:
    ConfigTempScreenController();
    void init();
    void redrawConfigureTempScreen();
    void updateTemperatureValue();
};

extern ConfigTempScreenController* configTempScreenController;

#endif