#ifndef BOL_MainScreenController_h
#define BOL_MainScreenController_h

// #include "TemperatureSensor.h"

class TemperatureSensor;

class MainScreenController {
  public:
    MainScreenController();
    void init();
    void redrawMainScreen();
    void updateLight();
    void updateTime();
    void updateTemperatureValue(TemperatureSensor* temperatureSensor);
    void updateHumidity();
};

extern MainScreenController* mainScreenController;

#endif