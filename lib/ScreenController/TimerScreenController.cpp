#include <TimerScreenController.h>
#include <NextionComponents.h>
#include <TimerController.h>
#include <MainScreenController.h>

TimerScreenController* timerScreenController = new TimerScreenController();

void resetTimerHandler(void *timerControllerPtr) {
    timerController -> resetTimer();
    mainPage.show();
    mainScreenController -> redrawMainScreen();
}

void cancelResetTimerHandler(void *timerControllerPtr) {
    mainPage.show();
    mainScreenController -> redrawMainScreen();
}

TimerScreenController::TimerScreenController() {

}

void TimerScreenController::init() {
    resetTimerBtn.attachPop(resetTimerHandler);
    cancelResetTimerBtn.attachPop(cancelResetTimerHandler);
}
