#ifndef BOL_TimerScreenController_h
#define BOL_TimerScreenController_h

class TimerScreenController {
  public:
    TimerScreenController();
    void init();
};

extern TimerScreenController* timerScreenController;

#endif