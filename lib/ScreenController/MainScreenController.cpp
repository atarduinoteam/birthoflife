#include <MainScreenController.h>
#include <NextionComponents.h>
#include <LightController.h>
#include <CommonConfiguration.h>
#include <HysteresisRound.h>
#include <EepromConfig.h>
#include <ConfigTempScreenController.h>
#include <TemperatureSensor.h>
#include <RtcController.h>
#include <HumidityController.h>

MainScreenController* mainScreenController = new MainScreenController();

void lightBtnHandler(void* ptr) {
    lightController -> lightSwitch();
    mainScreenController -> updateLight();
}

void tempConfigBtnHandler(void* ptr) {
    tempConfPage.show();
    configTempScreenController -> redrawConfigureTempScreen();
}

MainScreenController::MainScreenController() {

}

void MainScreenController::init() {
    lightBtn.attachPop(lightBtnHandler);
    tempConfBtn.attachPop(tempConfigBtnHandler, this);
    redrawMainScreen();
}

void MainScreenController::redrawMainScreen() {
    updateLight();
    updateTime();
    updateTemperatureValue(t1);
    // updateTemperatureValue(t2);
    // updateTemperatureValue(tEgg);
    char* configuredTemperatureStr = convertSensorValueToStr(eepromConfig -> getPresetTemp());
    tSetText.setText(configuredTemperatureStr);
    delete[] configuredTemperatureStr;
    updateHumidity();
    char* configuredHumidityStr = convertSensorValueToStr(eepromConfig -> getPresetHumidity());
    hSetTxt.setText(configuredHumidityStr);
    delete[] configuredHumidityStr;
}

void MainScreenController::updateLight() {
    lightBtn.Set_background_crop_picc(lightController -> isLightOn() ? MAIN_ON_PICC : MAIN_OFF_PICC);
}

void MainScreenController::updateTime() {
    timerText.setText(rtcController -> getTimerStrValue());
    dateTimeText.setText(rtcController -> getDateTimeStrValue());    
}

void MainScreenController::updateTemperatureValue(TemperatureSensor* temperatureSensor) {
    TemperatureSensorType temperatureSensorType = temperatureSensor -> getTemperatureSensorType();
    char* temperatureValueStr = temperatureSensor -> getValue();
    switch (temperatureSensorType) {
    case T1:
        t1Text.setText(temperatureValueStr);
        break;
    case T2:
        t2Text.setText(temperatureValueStr);
        break;
    case T_EGG:
        tEggText.setText(temperatureValueStr);
        break;
    }
    delete[] temperatureValueStr;
}

void MainScreenController::updateHumidity() {
    char* humidityStr = humidityController -> getValue();
    hTxt.setText(humidityStr);
    delete[] humidityStr;
}
