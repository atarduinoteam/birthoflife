#ifndef CommonConfiguration_h
#define CommonConfiguration_h

//PINs configuration
    #define LIGHT_PIN 7
    #define HEAT_PIN 10
    #define INT_FAN_PIN 11
    #define HUM_FAN_PIN 5
    #define EXT_FAN_PIN 12

// Temperature configuration
    #define DEFAULT_TEMPERATURE_TARGET_POINT 3550
    #define TEMPERATURE_KP 4.0
    #define TEMPERATURE_KI 0.05
    #define TEMPERATURE_KD 0.7
    #define TEMPERATURE_DT 5000

// Humidity configuration
    #define DEFAULT_HUMIDITY_TARGET_POINT 5000
    #define HUMIDITY_KP 1.0
    #define HUMIDITY_KI 0.02
    #define HUMIDITY_KD 0.0
    #define HUMIDITY_DT 2000

// Aeration configuration
    #define DEFAULT_AERATION_PERIOD 240 * 60000
    #define DEFAULT_AERATION_DURATION 60000

// Nextion constants 

    #define MAIN_OFF_PICC 0
    #define MAIN_ON_PICC 1

// Software version

    #define SOFTWARE_VERSION 2

#endif