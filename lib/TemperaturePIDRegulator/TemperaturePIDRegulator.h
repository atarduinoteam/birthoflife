#ifndef BOL_TemperaturePIDRegulator_h
#define BOL_TemperaturePIDRegulator_h

#include <LowFrequencyPWM.h>
#include <PIDRegulator.h>

/**
 * errorCode
 * 0 - no error;
 * 1 - t1 sensor and t2 sensor have difference more than 2 degrees
*/
class TemperaturePIDRegulator : public PIDRegulator {
  public:
    TemperaturePIDRegulator();
    void process() override;
  private:
    int getSensorValue() override;
    void control(int newControlSignal) override;
    void controlFan(int regulation);
    int _errorCode;
    LowFrequencyPWM* _lowFrequencyPWM;
};

extern TemperaturePIDRegulator* temperaturePIDRegulator;

#endif
