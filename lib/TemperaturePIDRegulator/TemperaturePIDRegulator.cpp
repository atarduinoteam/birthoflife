#include <TemperaturePIDRegulator.h>
#include <CommonConfiguration.h>
#include <TemperatureSensor.h>

TemperaturePIDRegulator* temperaturePIDRegulator = new TemperaturePIDRegulator();

TemperaturePIDRegulator::TemperaturePIDRegulator() : PIDRegulator() {
    _errorCode = 0;
    _lowFrequencyPWM = new LowFrequencyPWM(HEAT_PIN, 20, 250, true);
    setTargetPoint(DEFAULT_TEMPERATURE_TARGET_POINT);
    pinMode(INT_FAN_PIN, OUTPUT);
    digitalWrite(INT_FAN_PIN, LOW);
}

void TemperaturePIDRegulator::process() {
    PIDRegulator::process();
    _lowFrequencyPWM -> process();
}

int TemperaturePIDRegulator::getSensorValue() {
    Serial.println(t2 -> getNumericValue());
    return t2 -> getNumericValue();
}

void TemperaturePIDRegulator::control(int newControlSignal) {
    int regulation = _errorCode == 0 ? newControlSignal : 0;
    //Serial.print(_targetPoint);Serial.print(",");Serial.println(currentTemp2);//Serial.print(",");Serial.println(regulation*15);
    controlFan(regulation);
    _lowFrequencyPWM -> setDutyCycle(regulation);
}

void TemperaturePIDRegulator::controlFan(int regulation) {
    if (regulation == 0) {
        digitalWrite(INT_FAN_PIN, LOW);
    } else {
        digitalWrite(INT_FAN_PIN, HIGH);
    }
}
