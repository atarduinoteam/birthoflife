#include <HumidityPIDRegulator.h>
#include <HumidityController.h>
#include <CommonConfiguration.h>

HumidityPIDRegulator* humidityPIDRegulator = new HumidityPIDRegulator();

HumidityPIDRegulator::HumidityPIDRegulator() : PIDRegulator() {
    setTargetPoint(DEFAULT_HUMIDITY_TARGET_POINT);
    pinMode(HUM_FAN_PIN, OUTPUT);
    analogWrite(HUM_FAN_PIN, 0);
}

int HumidityPIDRegulator::getSensorValue() {
    // Serial.println(humidityController -> getNumericValue());
    return humidityController -> getNumericValue();
}

void HumidityPIDRegulator::control(int newControlSignal) {
    pinMode(HUM_FAN_PIN, OUTPUT);
    analogWrite(HUM_FAN_PIN, newControlSignal);
}