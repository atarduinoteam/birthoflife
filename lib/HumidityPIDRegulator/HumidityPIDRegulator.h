#ifndef BOL_HumidityPIDRegulator_h
#define BOL_HumidityPIDRegulator_h

#include <PIDRegulator.h>

class HumidityPIDRegulator : public PIDRegulator {
  public:
    HumidityPIDRegulator();
  private:
    int getSensorValue() override;
    void control(int newControlSignal) override;
};

extern HumidityPIDRegulator* humidityPIDRegulator;

#endif