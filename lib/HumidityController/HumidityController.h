#ifndef BOL_HumidityController_h
#define BOL_HumidityController_h

#define HUMIDITY_READ_PERIOD 15000

#include <BME280I2C.h>

class HumidityController {
    public:
        HumidityController();
        void init();
        void process();
        char* getValue();
        int getNumericValue();
    private:
        BME280I2C _bme;
        unsigned long _startTime;
        int _value;
};

extern HumidityController* humidityController;

#endif