#include <HumidityController.h>
#include <HysteresisRound.h>
#include <MainScreenController.h>

HumidityController* humidityController = new HumidityController();

HumidityController::HumidityController() {
    _value = 0;
}

void HumidityController::init() {
    BME280I2C::Settings settings(
        BME280::OSR_X16,
        BME280::OSR_X16,
        BME280::OSR_X1,
        BME280::Mode_Normal,
        BME280::StandbyTime_1000ms,
        BME280::Filter_Off,
        BME280::SpiEnable_False,
        BME280I2C::I2CAddr_0x76 // I2C address. I2C specific.
    );
    _bme = BME280I2C(settings);
    _bme.begin();
}

void HumidityController::process() {
    if (millis() - _startTime > HUMIDITY_READ_PERIOD) {
        mainScreenController -> updateHumidity();
        _startTime = millis();
    }
}

char* HumidityController::getValue() {
    float pressure(0), temperature(0), humidity(0);
    _bme.read(pressure, temperature, humidity, BME280::TempUnit_Celsius, BME280::PresUnit_Pa);
    int normalizedHumidity = humidity * 100;
    _value = hysteresisRound(_value, normalizedHumidity);
    return convertSensorValueToStr(_value / 10);
}

int HumidityController::getNumericValue() {
    return _value;
}
