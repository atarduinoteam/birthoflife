#include <HysteresisRound.h>
#include <Arduino.h>

int hysteresisRound(int prevValue, int newValue) {
    if (prevValue < 0 || newValue < 0) {
        return 0;
    }
    int units = newValue % 10;
    int minPossibleValue = newValue - units;
    int maxPossibleValue = newValue + 10 - units;
    int result;
    if (units < 4) {
        result = minPossibleValue;
    } else if (units > 5) {
        result = maxPossibleValue;
    } else {
        if (prevValue == minPossibleValue || prevValue == maxPossibleValue) {
            result = prevValue;
        } else if (units == 4) {
            result = minPossibleValue;
        } else if (units == 5) {
            result = maxPossibleValue;
        }
    }
    return result;
}

char* convertSensorValueToStr(int value) {
    char* strValue = new char[5];
    memset(strValue, 0, 5);
    sprintf(strValue, "%2d.%1d", value/10, value%10);
    return strValue;
}