#ifndef BOL_HysteresisRound_h
#define BOL_HysteresisRound_h

/**
 * hysteresisRound method rounds to tens by following rules:
 * units = 0 .. 3 => rounds units to 0;
 * units = 6 .. 9 => rounds units to 0 and increases tens by 1;
 * units = 4 .. 5 => previous value used, in case of it equals to tens or tens + 1; otherwise used regular round rules;
 * 
 * Method works only with positive values. If income values are negative, method returns 0;
*/
int hysteresisRound(int prevValue, int newValue);

/**
 * Convert 3-digit int value to 4-digit float string, where first two digits are tens, third digit is dot delimeter, and last one digit are units.
*/
char* convertSensorValueToStr(int value);

#endif