#include <RtcController.h>
#include <DS3232RTC.h>
#include <EepromConfig.h>
#include <MainScreenController.h>

RtcController* rtcController = new RtcController();

bool _interrupted = false;

void _timerInterrupt() {
    detachInterrupt(0);
    _interrupted = true;
}

RtcController::RtcController() {

}

void RtcController::init() {
    pinMode(2, INPUT_PULLUP);
    time_t timeToSet = _compileTime();
    time_t currentTime =  RTC.get();
    if (timeToSet > currentTime) {
        RTC.set(timeToSet);
    }
    if (eepromConfig -> getStartTime() == 0) {
        eepromConfig -> setStartTime(timeToSet);
    }
    RTC.setAlarm(ALM1_MATCH_SECONDS, 0, 0, 0, 0);
    RTC.clearAlarm(ALARM_1);
    RTC.alarmInterrupt(ALARM_1, true);
    attachInterrupt(0, _timerInterrupt, LOW);
}

void RtcController::process() {
    if (_interrupted) {
        _interrupted = false;
        RTC.clearAlarm(ALARM_1);
        mainScreenController -> updateTime();
        attachInterrupt(0, _timerInterrupt, LOW);
    }
}

char* RtcController::getTimerStrValue() {
    time_t timePeriod = RTC.get() - eepromConfig -> getStartTime();
    tmElements_t timePeriodTimeStructure;
    breakTime(timePeriod, timePeriodTimeStructure);
    memset(_timerStrValue, 0, sizeof(_timerStrValue));
    sprintf(_timerStrValue, "%03d:%02d:%02d",
            ((int) elapsedDays(timePeriod)) % 1000,
            timePeriodTimeStructure.Hour,
            timePeriodTimeStructure.Minute);
    return _timerStrValue;
}

char* RtcController::getDateTimeStrValue() {
    time_t now = RTC.get();
    tmElements_t nowTimeStructure;
    breakTime(now, nowTimeStructure);
    memset(_dateTimeStrValue, 0, sizeof(_dateTimeStrValue));
    sprintf(_dateTimeStrValue, "%02d/%02d/%02d %02d:%02d", 
            nowTimeStructure.Day, 
            nowTimeStructure.Month,
            tmYearToY2k(nowTimeStructure.Year),
            nowTimeStructure.Hour, 
            nowTimeStructure.Minute);
    return _dateTimeStrValue;
}

time_t RtcController::_compileTime() {
    const time_t FUDGE(10);    //fudge factor to allow for upload time, etc. (seconds, YMMV)
    const char *compDate = __DATE__, *compTime = __TIME__, *months = "JanFebMarAprMayJunJulAugSepOctNovDec";
    char compMon[4], *m;

    strncpy(compMon, compDate, 3);
    compMon[3] = '\0';
    m = strstr(months, compMon);

    tmElements_t tm;
    tm.Month = ((m - months) / 3 + 1);
    tm.Day = atoi(compDate + 4);
    tm.Year = atoi(compDate + 7) - 1970;
    tm.Hour = atoi(compTime);
    tm.Minute = atoi(compTime + 3);
    tm.Second = atoi(compTime + 6);

    time_t t = makeTime(tm);
    return t + FUDGE;        //add fudge factor to allow for compile time
}
