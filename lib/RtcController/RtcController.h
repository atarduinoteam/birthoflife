#ifndef BOL_RtcController_h
#define BOL_RtcController_h

#include <TimeLib.h>

class RtcController {
  public:
    RtcController();
    void init();
    void process();
    char* getTimerStrValue();
    char* getDateTimeStrValue();
  private:
    time_t _compileTime();
    char _timerStrValue[10] = {0};
    char _dateTimeStrValue[15] = {0};
};

extern RtcController* rtcController;

#endif