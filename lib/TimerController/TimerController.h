#ifndef BOL_TimerController_h
#define BOL_TimerController_h

class TimerController {
  public:
    TimerController();
    void resetTimer();
};

extern TimerController* timerController;

#endif
