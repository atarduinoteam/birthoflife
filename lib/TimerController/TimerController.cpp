#include <TimerController.h>
#include <EepromConfig.h>
#include <DS3232RTC.h>

TimerController* timerController = new TimerController();

TimerController::TimerController() {
    
}

void TimerController::resetTimer() {
    eepromConfig -> setStartTime(RTC.get());
}
